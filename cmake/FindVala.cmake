# - Find Vala
# This module looks for valac.
# This module defines the following values:
#    VALA_FOUND
#    VALA_COMPILER
#    VALA_VERSION

#=============================================================================
# Copyright (c) 2011, 2016, Přemysl Eric Janouch <p@janouch.name>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
# OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#=============================================================================

find_program (VALA_COMPILER "valac")

if (VALA_COMPILER)
	execute_process (COMMAND ${VALA_COMPILER} --version
		OUTPUT_VARIABLE VALA_VERSION)
	string (REGEX MATCH "[.0-9]+" VALA_VERSION "${VALA_VERSION}")
endif (VALA_COMPILER)

include (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (Vala
	REQUIRED_VARS VALA_COMPILER
	VERSION_VAR VALA_VERSION)

mark_as_advanced (VALA_COMPILER VALA_VERSION)

