[CCode (cheader_filename = "config.h", cprefix = "", lower_case_cprefix = "")]
namespace Config {
	public const string PROJECT_NAME;
	public const string PROJECT_VERSION;
	public const string SHARE_DIR;
}
